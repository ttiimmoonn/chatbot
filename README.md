# ChatBot

ChatBot это сервис для общения с ботом или другими участниками в чате.

### Технологии
* [AioHTTP](https://docs.aiohttp.org/en/stable/) - асинхронный HTTP-клиент/сервер для asyncio и Python
* [PyTorch](https://pytorch.org/) - фреймворк машинного обучения с открытым исходным кодом!

А так же датасет диалогов от [ЯндексТолока](https://toloka.ai/ru/datasets).

### Установка и запуск
Создайте новую виртуальную среду и установить зависимости
```sh
$ py -m venv chatbot
$ .\env\Scripts\activate
$ pip install -r requirements.txt
```
В корень пректа добавьте config.ini файл с конфигурацией сервера:
```ini
[SERVER]
host = localhost
port = 8080
```
Запустите сервер бекенда.
```sh
$ py main.py
```
