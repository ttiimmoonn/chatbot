import React, { useState, useEffect } from 'react';
import { FlatList, StyleSheet, Text, View, TextInput, Button } from 'react-native';

const client = new WebSocket('ws://192.168.0.5:8080/ws');

const styleColor = StyleSheet.create({
  lightPink: {
    backgroundColor: "rgba(249, 234, 255, 0.84)"
  },
  lightBlue: {
    backgroundColor: "rgba(218, 236, 255, 0.84)"
  }
})

const messageStyle = StyleSheet.create({
  message: {
    flex: 1,
    marginTop: 10,
  },
  messageHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 15
  },
  messageText: {
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,

    fontSize: 18,
    borderRadius: 15
  }
})

const inputStype = StyleSheet.create({
  input: {
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,

    fontSize: 18,
    height: 40
  }
})

const containerStyle = StyleSheet.create({
  containerView: {
    flex: 1,
    paddingTop: 22,
    paddingHorizontal: 10
   },

  messageList: {
    paddingBottom: 10
  }
})

const Message = (props) => {
  return (
    <View style={messageStyle.message}>
      <Text style={messageStyle.messageHeader}>{props.name}</Text>
      <Text style={[messageStyle.messageText, styleColor.lightBlue]}>{props.text}</Text>
    </View>
  );
}

export default function App() {
  const userName = 'Tim'
  var initMessageList = []

  const [messageText, setMessageText] = useState('');
  // Список сообщений
  const [messageList, setMessageList] = useState(initMessageList);

  const sendMesage = (event) => {
    client.send(`{"name": "${userName}", "message": "${messageText}"}`);
      setMessageText('');
  }

  const keyPress = (event) => {
    if (event.key === 'Enter' || event.charCode === 13) {
      sendMesage()
    }
  }

  client.onopen = () => {
    console.log('Открытие сокета.')
  };

  client.onerror = event => {
    console.log(event.message);
  };
  
  client.onclose = event => {
    console.log(event.code, event.reason);
  };

  const renderMessage = ({item}) => {
    return <Message name={item.name} text={item.message}/>
  }
  
  client.onmessage = event => {
    // a message was received
    var msg = JSON.parse(event.data);
    var newMessageList = [...messageList , {name: msg.name, message: msg.message}];
    setMessageList(newMessageList);
  };

  return (
    <View style={containerStyle.containerView}>
      <FlatList style={containerStyle.messageList}
        data={messageList}
        renderItem={renderMessage}
      />
      <View>
        <TextInput
          style={inputStype.input}
          placeholder="Введите сообщение!"
          onChangeText={text => setMessageText(text)}
          value={messageText}
          onKeyPress={keyPress}
        />
        <Button 
          onPress={sendMesage}
          title='Отправить'
        />
      </View>
    </View>
  );
}
