""" Точка входа для запуска приложения."""

import sys
import asyncio

from aiohttp import web

from chatbot.core.web.init_application import init_application
from chatbot.core.logger import create_logger

__author__ = "Ушаков Т.Л."

__all__ = []

logger = create_logger('./log', 'chat_bot', 'DEBUG')
logger = create_logger('./log', 'web_socket', 'DEBUG')
logger = create_logger('./log', 'background_task', 'INFO')


def main() -> None:
    """
    Точка входа для запуска web приложения.
    Блокирующая функция.

    :return:
    """
    logger.info('Сервер запускается...')
    loop = asyncio.get_event_loop()
    application = loop.run_until_complete(init_application())

    web.run_app(application, port=8080)


if __name__ == '__main__':
    main()
