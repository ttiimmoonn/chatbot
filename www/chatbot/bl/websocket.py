""" Создаём вебсокет."""

import logging

from asyncio import sleep
from random import uniform

from aiohttp import web, WSMsgType
from aiohttp_session import get_session

from chatbot.core.web.business_logic import BusinessLogic
from chatbot.core.msg import Message

__author__ = 'Пашуков И.С.'

__all__ = ['WebSocket']

logger = logging.getLogger("web_socket")

# Пользователь по умолчанию.
DEFAULT_USER = "Admin"
# Приветствие по умолчанию.
DEFAULT_GREETING = "New user joined the chat..."
# Прощание по умолчанию.
DEFAULT_LEAVE = "User disconnected..."
# Максимальное количество символов в сообщении
LIMIT_MESSAGE = 2000

class WebSocket(BusinessLogic):
    """ Создаём вебсокет."""

    ROUTE = '/ws'
    METHOD = 'GET'

    async def get(self):
        """ Создаём вебсокет коннекцию."""
        ws = await self._get_ws_from_request()
        logger.debug('Открыт новый websocket.')
        async for msg in ws:
            if msg.type == WSMsgType.TEXT:
                if msg.data == 'close':
                    await ws.close()
                elif len(msg.data) > LIMIT_MESSAGE:
                    await ws.send_json(
                        Message(DEFAULT_USER, 'Слишком большое сообщение.').get_message_as_dict()
                    )
                else:
                    chat_message = Message.parser_json_message(msg.data)
                    await self._send_msg_in_all_ws(chat_message)
                    self.request.app['message_queue'].append(chat_message)
            elif msg.type == WSMsgType.ERROR:
                logger.error(f'В ws получено сообщение с ошибкой: {ws.exception()}')
        return await self._close_ws(ws)

    async def _send_msg_in_all_ws(self, msg: Message) -> None:
        """ Отправить сообщение во все подключенные вебсокеты."""
        for ws in self.request.app['websockets']:
            try:
                await ws.send_json(msg.get_message_as_dict())
            except ConnectionResetError:
                logger.error('Ошибка при попытке отправить сообщение в закрытый ws.')
                self.request.app['websockets'].remove(ws)

    async def _get_ws_from_request(self) -> web.WebSocketResponse:
        """ Получить вебсокет."""
        ws = web.WebSocketResponse()
        await ws.prepare(self.request)
        await self._send_msg_in_all_ws(Message(DEFAULT_USER, DEFAULT_GREETING))
        self.request.app['websockets'].append(ws)
        self.session = await get_session(self.request)
        return ws

    async def _close_ws(self, ws: web.WebSocketResponse) -> web.WebSocketResponse:
        """ Закрыть вебсокет."""
        self.request.app['websockets'].remove(ws)
        await self._send_msg_in_all_ws(Message(DEFAULT_USER, DEFAULT_LEAVE))
        logger.debug(f'Web socket "{ws}" закрыт.')
        return ws
