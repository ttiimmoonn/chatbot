""" Отправить сообщение."""

from aiohttp import web

from chatbot.core.web.business_logic import BusinessLogic

__all__ = ['SendMessage']


class SendMessage(BusinessLogic):
    """ Отправить сообщение."""

    ROUTE = '/api/v1/message/send'
    METHOD = 'GET'

    async def get(self):
        return web.StreamResponse(status=200, reason='OK')

