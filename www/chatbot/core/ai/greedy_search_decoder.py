from torch import nn, ones, long, zeros, max, cat, unsqueeze

from chatbot.core.ai.voc import Token


class GreedySearchDecoder(nn.Module):
    def __init__(self, encoder, decoder, device):
        super(GreedySearchDecoder, self).__init__()
        self.encoder = encoder
        self.decoder = decoder
        self.device = device

    def forward(self, input_seq, input_length, max_length):
        encoder_outputs, encoder_hidden = self.encoder(input_seq, input_length)
        decoder_hidden = encoder_hidden[:self.decoder.n_layers]
        decoder_input = ones(1, 1, device=self.device, dtype=long) * Token.SOS_token
        all_tokens = zeros([0], device=self.device, dtype=long)
        all_scores = zeros([0], device=self.device)

        for _ in range(max_length):
            decoder_output, decoder_hidden = self.decoder(decoder_input, decoder_hidden, encoder_outputs)
            decoder_scores, decoder_input = max(decoder_output, dim=1)
            all_tokens = cat((all_tokens, decoder_input), dim=0)
            all_scores = cat((all_scores, decoder_scores), dim=0)
            decoder_input = unsqueeze(decoder_input, 0)
        return all_tokens, all_scores
