""" Вокабуляр. Набор слов для AI."""


class Token:
    # Для заполнения коротких предложений
    PAD_token = 0
    # Токен начала предложения
    SOS_token = 1
    # ТОкен окончания предложения
    EOS_token = 2


class Voc:
    """ Вокабуляр для бота."""

    def __init__(self, name):
        """
        Конструктор для вокабуляра.
        :param name: Название вокабуляра.
        """
        self.name = name

        # Отфильтрованы ли слова
        self.trimmed = False
        # В ключах все известные слова, а в значениях индекс слова
        self.word2index = {}
        # В ключах все известные слова, а в значения количество раз, сколько они встречаются в исходных данных
        self.word2count = {}
        self.index2word = {Token.PAD_token: "PAD", Token.SOS_token: "SOS", Token.EOS_token: "EOS"}
        self.num_words = 3

    def add_sentence(self, sentence):
        """
        Добавить все слова из предложения в вокабуляр.

        :param sentence: Предложения на добавления в вокабуляр.
        """
        for word in sentence.split(' '):
            self.add_word(word)

    def add_word(self, word):
        """
        Добавить слово в вокабуляр.

        :param word: Слово для добавления в вокабуляр.
        """
        if word not in self.word2index:
            self.word2index[word] = self.num_words
            self.word2count[word] = 1
            self.index2word[self.num_words] = word
            self.num_words += 1
        else:
            self.word2count[word] += 1

    def trim(self, min_count):
        """
        Отфильтровать вокабуляр оставив только слова, которые используются
        больше раз чем заданное значение.

        :param min_count: Минимальное количество раз, сколько слово должно повторяться.
        """
        if self.trimmed:
            return
        self.trimmed = True

        keep_words = []

        for word, word_count in self.word2count.items():
            if word_count >= min_count:
                keep_words.append(word)

        self.word2index = {}
        self.word2count = {}
        self.index2word = {Token.PAD_token: "PAD", Token.SOS_token: "SOS", Token.EOS_token: "EOS"}
        self.num_words = 3

        for word in keep_words:
            self.add_word(word)
