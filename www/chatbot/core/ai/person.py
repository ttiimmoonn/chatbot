""" Боты в чате."""

from chatbot.core.msg import Message
from chatbot.core.ai.ai import Ai

__author__ = 'Ушаков Т.Л.'

__all__ = ['Jack', 'Emily']


class Jack(Ai):
    """ Персонаж чата Джек."""
    name = 'Jack'


class Emily(Ai):
    """ Персонаж чата Эмили."""
    name = 'Emily'
