""" Метакласс для AI."""

import os

from torch import LongTensor, nn, device, load, cuda, tensor

from chatbot.core.ai.voc import Voc, Token
from chatbot.core.ai.encoder import EncoderRNN
from chatbot.core.ai.decoder import LongAttnDecoderRNN
from chatbot.core.ai.string_handler import normalize_string
from chatbot.core.ai.data_tokenization import indexes_from_sentence
from chatbot.core.ai.greedy_search_decoder import GreedySearchDecoder

__author__ = 'Ушаков Т.Л.'

__all__ = ['Ai']

MODULE_PATH = os.path.dirname(__file__)
device = device("cuda" if cuda.is_available() else "cpu")


class Ai:
    """ Метакласс AI."""

    # Директория с моделью бота
    CURRENT_PATH = MODULE_PATH
    # Директория с моделями
    MODEL_DIR = 'training_checkpoints'
    # Название модели
    MODEL_NAME = 'ru_checkpoint.tar'
    # Максимальная длина предложения для рассмотрения
    MAX_LENGTH = 100
    # Размер каждого вектора вложения
    HIDDEN_SIZE = 500
    # Название модели предсказания текста
    ATTN_MODEL = 'dot'
    # Количество уровней кодировщика в модели
    ENCODE_N_LAYERS = 2
    # Количество уровней декодера в модели
    DECODE_N_LAYERS = 2
    DROPOUT = 0.1

    def __init__(self):
        """ Конструктор."""
        self.voc = Voc(self.MODEL_DIR)
        self.checkpoint = load(os.path.join(self.CURRENT_PATH, self.MODEL_DIR, self.MODEL_NAME))

        self.voc.__dict__ = self.checkpoint['voc_dict']

        self.embedding = nn.Embedding(self.voc.num_words, self.HIDDEN_SIZE)
        self.embedding.load_state_dict(self.checkpoint['embedding'])

        self.encoder = EncoderRNN(self.HIDDEN_SIZE, self.embedding, self.ENCODE_N_LAYERS, self.DROPOUT)
        self.encoder.load_state_dict(self.checkpoint['en'])

        self.decoder = LongAttnDecoderRNN(
            self.ATTN_MODEL, self.embedding, self.HIDDEN_SIZE, self.voc.num_words, self.DECODE_N_LAYERS, self.DROPOUT
        )
        self.decoder.load_state_dict(self.checkpoint['de'])

        self.encoder.to(device).eval()
        self.decoder.to(device).eval()

        self.searcher = GreedySearchDecoder(self.encoder, self.decoder, device)

    def _evaluate(self, message: str) -> list:
        """
        Получить набор слов ответа от Ai.

        :param message: Сообщение для которого необходимо сгенерировать ответ.
        :return: Список слова ответа от Ai.
        """
        # Перевести слова из строки в набор токенов.
        indexes_batch = [indexes_from_sentence(self.voc, message)]
        # Получить длину тензора
        lengths = tensor([len(indexes) for indexes in indexes_batch])
        # Transpose dimensions of batch to match models' expectations
        input_batch = LongTensor(indexes_batch).transpose(0, 1)
        # Используйте соответствующее устройство
        input_batch = input_batch.to(device)
        lengths = lengths.to(device)
        # Получить набор токенов в ответ на сообщение
        tokens, scores = self.searcher(input_batch, lengths, self.MAX_LENGTH)
        # Преобразуем из токенов в набор слов
        decoded_words = [self.voc.index2word[token.item()] for token in tokens]
        return decoded_words

    def get_response(self, message: str) -> str:
        """
        Получить ответ на сообщение.

        :param message: Сообщение для которого необходимо сгенерировать ответ.
        :return: Текс ответа на сообщение.
        """
        normalize_message = normalize_string(message)

        output_words = [x for x in self._evaluate(normalize_message) if not (
                x == self.voc.index2word[Token.EOS_token] or x == self.voc.index2word[Token.PAD_token]
        )]
        return ' '.join(output_words)
