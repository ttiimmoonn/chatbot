""" Подготовка и обработка строк."""

import re
import unicodedata

# Шаблоны для подготовки текста.
NORMALIZE_PATTERNS = [
    (r"([.!?])", r" \1"),
    (r"[^a-zA-ZА-я.!?]+", r" "),
    (r"\s+", r" ")
]


def unicode_to_ascii(text: str) -> str:
    """
    Перевод кодировки из unicode в ascii.

    :param text: Текст в unicode.
    :return: Текст в ascii.
    """
    return ''.join(
        symbol for symbol in unicodedata.normalize('NFD', text)
        if unicodedata.category(symbol) != 'Mn'
    )


def normalize_string(text: str) -> str:
    """
    Подготовка строки для Ai.

    :param text: Текст для обработки.
    :return: Подготовленный текст.
    """
    text = unicode_to_ascii(text.lower().strip())
    for pattern, re_pattern in NORMALIZE_PATTERNS:
        text = re.sub(pattern, re_pattern, text)
    return text.strip()
