""" Работа с токенами слов."""

import itertools

from torch import LongTensor, BoolTensor, tensor

from chatbot.core.ai.voc import Token


def indexes_from_sentence(voc, sentence):
    return [voc.word2index[word] for word in sentence.split(' ')] + [Token.EOS_token]


def zero_padding(items, fill_value=Token.PAD_token):
    return list(itertools.zip_longest(*items, fillvalue=fill_value))


def binary_matrix(tokens):
    result = []
    for index, seq in enumerate(tokens):
        result.append([])
        for token in seq:
            if token == Token.PAD_token:
                result[index].append(0)
            else:
                result[index].append(1)
    return result


def input_var(words, voc):
    """
    Получить заполненный тензор входной последовательности и длину.

    :param words: Массив слов.
    :param voc: Вокобуляр.
    """
    indexes_batch = [indexes_from_sentence(voc, sentence) for sentence in words]
    lengths = tensor([len(indexes) for indexes in indexes_batch])
    pad_list = zero_padding(indexes_batch)
    pad_var = LongTensor(pad_list)
    return pad_var, lengths


def output_var(words, voc):
    """
    Возвращает заполненный тензор целевой последовательности,
    маску заполнения и максимальную длину цели.

    :param words: Массив слов.
    :param voc: Вокобуляр.
    """
    indexes_batch = [indexes_from_sentence(voc, sentence) for sentence in words]
    max_target_len = max([len(indexes) for indexes in indexes_batch])
    pad_list = zero_padding(indexes_batch)
    mask = binary_matrix(pad_list)
    mask = BoolTensor(mask)
    pad_var = LongTensor(pad_list)
    return pad_var, mask, max_target_len


def batch2_train_data(voc, pair_batch):
    """
    Возвращает все элементы для данной партии пар

    :param voc: Вокобуляр.
    :param pair_batch: Набор слов.
    """
    pair_batch.sort(key=lambda x: len(x[0].split(" ")), reverse=True)
    input_batch, output_batch = [], []
    for pair in pair_batch:
        input_batch.append(pair[0])
        output_batch.append(pair[1])
    inp, lengths = input_var(input_batch, voc)
    output, mask, max_target_len = output_var(output_batch, voc)
    return inp, lengths, output, mask, max_target_len
