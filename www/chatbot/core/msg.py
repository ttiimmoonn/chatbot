""" Модуль сообщений."""

import json

from datetime import datetime

__author__ = "Ушаков Т.Л."

__all__ = ['Message']


class Message:
    """ Класс сообщения."""

    def __init__(self, name=str(), message=str(), date_time=datetime.now()) -> None:
        """
        Конструктор сообщения.

        :param name: Автор сообщения.
        :param message: Текст сообщения.
        :param date_time: Дата и время сообщения.
        """
        self.name = name
        self.message = message
        self.date_time = date_time

    def get_message_as_json(self) -> str:
        """
        Получить Json строку сообщения.

        :return: Возвращает Json строку.
        """
        return f'{"name": "{self.name}", "message": "{self.message}"}'

    def get_message_as_dict(self) -> dict:
        """ Получить словарь из параметров сообщения."""
        return {"name": self.name, "message": self.message}

    @staticmethod
    def parser_json_message(message_str):
        """
        Получить объект Message из строки json.

        :return: Возвращает Message class если строка является Json строкой и None, если нет.
        """
        try:
            message_dict = json.loads(message_str)
        except json.decoder.JSONDecodeError:
            return None

        return Message(message_dict.get('name'), message_dict.get('message'), message_dict.get('date_time'))
