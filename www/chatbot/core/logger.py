""" Модуль логгирования. """

import os
import logging


__author__ = 'Пашуков И.С.'

__all__ = ['create_logger']

DEFAULT_FORMATTER = logging.Formatter("%(asctime)-8s %(levelname)-6s [%(name)-2s|%(module)s:%(lineno)d] %(message)-8s")

LOG_LEVEL_TYPE = {
    'INFO': logging.INFO,
    'DEBUG': logging.DEBUG,
    'ERROR': logging.ERROR
}


def create_logger(path: str, log_name='chat_bot', level='INFO'):
    """ Получить логгер."""
    if not os.path.exists(path):
        os.makedirs(path)

    logger = logging.getLogger(log_name)
    logger.setLevel(LOG_LEVEL_TYPE[level])

    logger.addHandler(_get_logger_handler_stream(LOG_LEVEL_TYPE[level]))

    logger.addHandler(_get_logger_handler_file(path, LOG_LEVEL_TYPE[level]))

    return logger


def _get_logger_handler_file(log_path, log_level, log_formatter=DEFAULT_FORMATTER):
    """ Получить файловый логгер обработчик ошибок."""
    return _get_logger_handler(logging.FileHandler(f'{log_path}/log_{log_level}.log', 'w'), log_level, log_formatter)


def _get_logger_handler_stream(log_level, log_formatter=DEFAULT_FORMATTER):
    """ Получить консольный логгер обработчик ошибок."""
    return _get_logger_handler(logging.StreamHandler(), log_level, log_formatter)


def _get_logger_handler(handler, log_level, log_formatter):
    """ Получить log handler."""
    handler.setLevel(log_level)
    handler.setFormatter(log_formatter)
    return handler
