""" Реализация бота, который отвечает на сообщения."""

import logging

from aiohttp import web
from asyncio import sleep

from chatbot.core.msg import Message
from chatbot.core.ai.person import Jack, Emily

__all__ = ['await_message']

__author__ = 'Ушаков Т.Л.'

logger = logging.getLogger("background_task")

# Количество секунд между проверкой сообщений.
AWAIT_MESSAGE_TIMEOUT = 3
# Количество итераций ожиданая после которого боты начнут общаться между собой.
NUMBER_DOWNTIME = 3
# Боты которые общаются в чате.
BOTS = [Emily(), Jack()]
# Фраза по умолчанию, чтобы начать диалог ботам.
START_PHRASE = 'Как дела?'


async def await_message(app: web.Application) -> None:
    """
    Бесконечно вычитываем из приложения сообщения для бота. Если сообщения нет, то ждем
    и проверяем очередь заново.

    :param app: Web приложение.
    """
    logger.debug(
        'Запуск ботов в фоновом режиме: '
        f'{", ".join([bot.name for bot in BOTS])}...'
    )

    last_bot = BOTS[0]
    last_bot_response = Message(last_bot.name, last_bot.get_response(START_PHRASE))

    # Итерация ожидания сообщений в чате
    wait_iteration = 0
    end_listen = False

    while not end_listen:
        await sleep(AWAIT_MESSAGE_TIMEOUT)

        # В очереди нет сообщений
        if not app['message_queue']:
            if wait_iteration < NUMBER_DOWNTIME:
                wait_iteration += 1
                logger.debug(
                    'Нет сообщений в очереди, ожидание... '
                    f'{wait_iteration}, {NUMBER_DOWNTIME}'
                )
                continue

            logger.debug(
                'Нет сообщений в очереди. Боты генерируют общения...'
                f' {wait_iteration}, {NUMBER_DOWNTIME}'
            )
            try:
                response = last_bot.get_response(last_bot_response.message)
            except Exception:
                logger.error('Бот не смог получить ответ')
                response = 'Я не понимаю тебя'
            finally:
                message_response = Message(
                        last_bot.name,
                        response
                    )
            last_bot = choose_bot(last_bot)
            app['message_queue'].append(message_response)
            await _send_msg_in_all_ws(app['websockets'], message_response)
            continue

        message_queue = app['message_queue'].copy()
        app['message_queue'].clear()
        wait_iteration = 0

        logger.debug(
            'В очереди есть сообщения.'
            f' Количество: "{len(message_queue)}"...'
        )

        request_bot = ' '.join(map(lambda msg: msg.message, message_queue))
        try:
            response = last_bot.get_response(request_bot)
        except Exception:
            logger.error('Бот не смог получить ответ')
            response = 'Я не понимаю тебя'
        finally:
            last_bot_response = Message(
                    last_bot.name,
                    response
                )
        await _send_msg_in_all_ws(app['websockets'], last_bot_response)


async def _send_msg_in_all_ws(web_socket: list, msg: Message) -> None:
    """
    Отправить сообщение во все подключенные вебсокеты.

    :param web_socket: Список web сокетов.
    :param msg: Сообщение.
    """
    logger.debug(f'Бот "{msg.name}" отвечает всем клиентам: "{msg.message}"...')
    for ws in web_socket:
        try:
            await ws.send_json(msg.get_message_as_dict())
        except ConnectionResetError:
            logger.error('Ошибка при попытке отправить сообщение в закрытый ws.')
            web_socket.remove(ws)


def choose_bot(last_bot: [Emily, Jack]) -> [Emily, Jack]:
    if isinstance(last_bot, Jack):
        return BOTS[0]
    else:
        return BOTS[1]
