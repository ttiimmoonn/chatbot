""" Модуль для формирования настроек для сервера."""

import logging

from aiohttp import web
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from aiohttp_session import setup


from chatbot.core.web.route import setup_routes
from chatbot.core.web.background_task import setup_background_task

__author__ = 'Ушаков Т.Л.'

__all__ = ['init_application']

logger = logging.getLogger("chat_bot")


async def init_application() -> web.Application:
    """
    Инициализация web приложения.

    :return: web приложение.
    """
    logger.info('Инициализация приложения...')
    application = web.Application()
    application['websockets'] = list()
    application['message_queue'] = list()

    setup(application,
          EncryptedCookieStorage(b'Thirty  two  sength  bytes  key.'))

    setup_routes(application)
    setup_background_task(application)

    return application
