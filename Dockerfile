FROM python:3.8-slim-buster

# Устанавливаем рабочую директорию
WORKDIR /server

# Создадим папку для логов
RUN mkdir log

# Установка зависимостей
ADD requirements.txt .
RUN ["python3.8", "-m", "pip", "install", "--no-cache-dir", "-r", "requirements.txt"]
RUN ["python3.8", "-m", "pip", "install", "--no-cache-dir", "torch==1.7.1+cpu", "torchvision==0.8.2+cpu", "-f", "https://download.pytorch.org/whl/torch_stable.html"]

# Копируем код в рабочу директорию
COPY www/ .

# Прокидываем рабочий порт
EXPOSE 8080

# Запускаем сервер
CMD ["python3.8", "main.py"]
