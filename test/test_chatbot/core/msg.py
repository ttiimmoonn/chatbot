import unittest

from chatbot.core.msg import Message


class TestMessage(unittest.TestCase):

    def test_init(self):
        new_message = Message('text')

        self.assertEqual(new_message.text, 'text')
        self.assertIsNotNone(new_message.date_time)
