import unittest

from chatbot.core.ai.ai import Ai


class TestAi(unittest.TestCase):

    def test_get_response(self):
        """ Проверяем, что у метакласса есть метод get_response."""
        with self.assertRaises(Exception):
            Ai.get_response('test')
