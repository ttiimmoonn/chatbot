import unittest

from parameterized import parameterized
from chatbot.core.ai.person import Emily, Jack


class TestPerson(unittest.TestCase):

    @parameterized.expand([
        (Emily, 'test'),
        (Emily, ''),
        (Emily, 1),
        (Emily, None),
        (Jack, 'test')
    ])
    def test_get_response(self, _class, message):
        """ Проверяем, что у классов работает метод get_response."""
        self.assertEqual(_class.get_response(message), f"Я {_class.__name__}. Не пиши мне больше '{message}'.")
