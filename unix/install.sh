#!/usr/bin/env bash

export WORK_DIR_CHATBOT=`pwd`

cp ${WORK_DIR_CHATBOT}/unix/chatbot.service /etc/systemd/system/

systemctl enable chatbot.service
systemctl start chatbot.service