// Интервал переподключения к вебсокету
var reconnectInterval = 3 * 1000;
// Адрес бекенда
var host = 'bluechili.ru';
var ws;


function connect() {
    ws = new WebSocket(`wss://${host}/ws`);

    ws.onclose = function(event) {
        console.log('Socket is closed. Reconnect will be attempted in 1 second.', event.reason);
        setTimeout(function() {
            connect();
        }, reconnectInterval);
    };
    
    ws.onerror = function(event) {
        console.error('Socket encountered error: ', event.message, 'Closing socket');
        ws.close();
    };
    
    ws.onmessage = function(event) {
        var msg = JSON.parse(event.data);
        if (msg.name === author) {
            chat_window.append(createMessage(msg.name, msg.message, 'their'));
        } else {
            chat_window.append(createMessage(msg.name, msg.message, 'another'));
        }
        chat_window.scrollTop = chat_window.scrollHeight;
    };
}

connect();

function send_message() {
    ws.send(`{"name": "${author}", "message": "${send.value}"}`);
    send.value = '';
};

var chat_window = document.getElementById('chat_window'),
    send = document.getElementById('send'),
    input_username = document.getElementById('input_username'),
    welcome_window = document.getElementById('welcome_window'),
    title = document.getElementById('title')

var author

function delete_welcome_window() {
    author = input_username.value;
    welcome_window.remove();
}

function change_color() {
    if (title.classList.contains('title') && input_username.classList.contains('username_field')) {
        title.classList.toggle('title');
        title.classList.toggle('title_empty');
        input_username.classList.toggle('username_field');
        input_username.classList.toggle('username_field_empty');
    }
}

function return_color() {
    if (title.classList.contains('title_empty') && input_username.classList.contains('username_field_empty')) {
        title.classList.toggle('title');
        title.classList.toggle('title_empty');
        input_username.classList.toggle('username_field');
        input_username.classList.toggle('username_field_empty');
    }
}


input_username.addEventListener("keydown", ({
    key
}) => {
    if (key === "Enter") {
        if (input_username.value.length < 3 || input_username.value.indexOf(' ') !== -1) {
            change_color();
        } else {
            delete_welcome_window();
        }
    }
});

input_username.addEventListener("input", function(e) {
    return_color()
});


function createMessage(author_text, message_text, type_message) {

    var message_div = document.createElement('div');
    var message_author_div = document.createElement('div');
    var message_text_box_div = document.createElement('div');
    var message_text_div = document.createElement('div');

    var pass_div = document.createElement('div');
    pass_div.className = 'text_box__flex_separator'

    message_author_div.className = `message__author author__${type_message}`;
    message_text_box_div.className = `flex_item message__text_box__${type_message}`;
    message_text_div.className = `text_box__text color_${type_message} text_box__${type_message}`;
    message_div.className = `flex_item message message__${type_message}`;

    message_author_div.textContent = author_text;
    message_text_div.textContent = message_text;

    message_text_box_div.prepend(message_text_div);
    message_div.prepend(message_text_box_div);
    message_div.prepend(message_author_div);

    if (type_message === 'their') {
        message_text_box_div.prepend(pass_div)
    } else {
        message_text_box_div.append(pass_div)
    }

    return message_div;
}

send.addEventListener("keydown", ({
    key
}) => {
    if (key === "Enter") {
        if (/[^\s]/gim.test(send.value)) {
            event.preventDefault();
            send_message();
        } else {

        }
    }
})
